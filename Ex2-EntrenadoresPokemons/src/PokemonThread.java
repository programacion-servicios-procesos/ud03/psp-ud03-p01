public class PokemonThread extends Thread implements Runnable {

    private int numeroPokemon;
    private Thread entrenador;

    public PokemonThread(int numeroPokemon, Thread entrenador) {
        this.numeroPokemon = numeroPokemon;
        this.entrenador = entrenador;
    }

    public void run() {
        entrenador.start();
        try {
            entrenador.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Soy el Pokémon número: " + numeroPokemon);
        }
    }
}
