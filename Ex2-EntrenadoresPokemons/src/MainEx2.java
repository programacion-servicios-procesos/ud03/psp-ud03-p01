import java.util.Scanner;

public class MainEx2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int seleccion;

        Thread entrenador = null;
        Thread pokemon = null;

        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println(" Introduce un número (Par para los entrenadores, e impar para los Pokemons). 0 para salir ");
        System.out.println("-----------------------------------------------------------------------------------------");

        do {
            System.out.println("Dame un número");
            seleccion = scanner.nextInt();

            if (seleccion > 0) {
                if (seleccion % 2 == 0) {
                    entrenador = new EntrenadorThread(seleccion);
                } else {
                    if (entrenador != null) {
                        pokemon = new PokemonThread(seleccion, entrenador);
                        pokemon.start();

                        try {
                            pokemon.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            entrenador = null;
                        }
                    } else {
                        System.out.println("Crea primero otro entrenador. ¡Los Pokémon nunca van solos!");
                    }
                }
            }
        } while (seleccion > 0);

        System.out.println("Hasta luego!");
    }
}
