public class MainEx3 {
    public static void main(String[] args) {

        final int THREAD_POOL = 10;
        final int CABALLO_CHETADO = 10;
        final int CABALLO_VIEJUNO = 1;

        ThreadHorse threadHorse = null;

        for (int i = 1; i <= THREAD_POOL; i++) {
            threadHorse = new ThreadHorse(i);

            if (i == CABALLO_CHETADO) {
                threadHorse.setPriority(Thread.MAX_PRIORITY);
            } else if (i == CABALLO_VIEJUNO) {
                threadHorse.setPriority(Thread.MIN_PRIORITY);
            } else {
                threadHorse.setPriority(Thread.NORM_PRIORITY);
            }
            threadHorse.start();
        }

        /**
         * Conclusión: Los caballos llegan cuando les da la gana.
         * Windows tiene su propio sistema para gestionar threads, por lo que aun configurando las prioridades
         * no notamos una diferencia reseñable.
         **/
    }
}



