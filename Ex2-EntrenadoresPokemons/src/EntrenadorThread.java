public class EntrenadorThread extends Thread {

    private int numeroEntrenador;

    public EntrenadorThread(int numeroEntrenador) {
        this.numeroEntrenador = numeroEntrenador;
    }

    public void run() {
        System.out.println("Soy el entrenador número: " + numeroEntrenador);
    }
}

