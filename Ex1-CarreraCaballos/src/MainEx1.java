public class MainEx1 {
    public static void main(String[] args) {

        final int THREAD_POOL = 10;

        ThreadHorse threadHorse = null;

        for (int i = 0; i < THREAD_POOL; i++) {
            threadHorse = new ThreadHorse(i + 1);
            threadHorse.start();
        }
    }
}


