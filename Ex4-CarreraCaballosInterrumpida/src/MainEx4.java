public class MainEx4 {
    public static void main(String[] args) {

        final int THREAD_POOL = 10;

        Thread[] threadHorseArray = new Thread[THREAD_POOL];

        for (int i = 0; i < threadHorseArray.length; i++) {
            threadHorseArray[i] = new ThreadHorse(i + 1);
            threadHorseArray[i].start();
        }

        JudgeThread judgeThread = new JudgeThread("JudgeThread", threadHorseArray);
        judgeThread.start();
    }
}


