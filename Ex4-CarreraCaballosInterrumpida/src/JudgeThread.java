import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class JudgeThread extends Thread implements Runnable {

    private Thread threadsHorses[];

    public JudgeThread(String name, Thread threadsHorses[]) {
        super(name);
        this.threadsHorses = threadsHorses;
    }

    @Override
    public void run() {
        super.run();

        Random random = new Random();
        String caballosDescalificados = "Caballos descalificados: ";
        Set<Integer> caballosDescalificadosNumeros = new HashSet<>();

        for (int i = 0; i < 5; i++) {
            int caballoTramposo = random.nextInt(9) + 1;

            if (!caballosDescalificadosNumeros.contains(caballoTramposo)) {
                caballosDescalificadosNumeros.add(caballoTramposo);

                caballosDescalificados += threadsHorses[caballoTramposo].getId() + ", ";

                try {
                    sleep(1700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                threadsHorses[caballoTramposo].interrupt();
                System.err.println(caballosDescalificados);
            } else {
                i--;
            }
        }
        System.err.println("Los caballos descalificados totales son: " + caballosDescalificados);
    }
}