public class Animal extends Thread implements Runnable {

    private int contador;
    private final int numeroThread;
    private final String nombre;
    private final int descanso;

    private final int METROS = 100;
    private final int LINEA_DE_META = 3000;

    public Animal(String nombre, int numeroThread, int descanso) {
        this.nombre = nombre;
        this.numeroThread = numeroThread;
        this.descanso = descanso;
    }

    public void run() {
        contador = 0;

        while (contador <= LINEA_DE_META) {
            if (LINEA_DE_META - contador != 0)
                System.out.println(nombre + " " + numeroThread + " : " + (LINEA_DE_META - contador) + "m para finalizar");

            contador += METROS;

            try {
                sleep(descanso);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (contador == LINEA_DE_META) {
                System.err.println(nombre + " " + numeroThread + " ha finalizado la carrera");
            }
        }
    }
}
