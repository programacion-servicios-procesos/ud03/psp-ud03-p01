public class ThreadHorse extends Thread {

    private int contador;
    private int numeroThread;
    private int METROS = 100;
    final int LINEA_DE_META = 5000;

    public ThreadHorse(int numeroThread) {
        this.numeroThread = numeroThread;
        System.out.println("El caballo " + numeroThread + " empieza la carrera...");
    }

    public void run() {
        contador = 0;

        while (contador <= LINEA_DE_META) {
            if (LINEA_DE_META - contador != 0)
                System.out.println("Caballo " + numeroThread + ": " + (LINEA_DE_META - contador) + "m para finalizar");

            contador += METROS;

            try {
                sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (contador == LINEA_DE_META) {
                System.out.println("Caballo " + numeroThread + " ha finalizado la carrera");
            }
        }
    }
}
