import java.util.InputMismatchException;
import java.util.Scanner;

public class MainEx5 {

    private static final int NUM_CONEJOS = 3;
    private static final int NUM_GUEPARDOS = 1;
    private static final int NUM_TORTUGAS = 4;

    /* Atributos para asegurar que los animales para asegurarnos que siempre lleguen en sus puestos */
    private static final int DESCANSO_CONEJO = 300;
    private static final int DESCANSO_GUEPARDO = 100;
    private static final int DESCANSO_TORTUGA = 500;

    public static void main(String[] args) {
        Thread[] conejos = new Animal[NUM_CONEJOS];
        Thread[] guepardos = new Animal[NUM_GUEPARDOS];
        Thread[] tortugas = new Animal[NUM_TORTUGAS];

        /** En la versión inicial del programa asignamos la velocidad de los animales por la prioridad de los threads
        pero en ejecución comprobamos que eso no es suficiente, y no se garantiza el resultado esperado.
         Por ello en la versión modificada del programa cambiamos los valores de sleep para cada animal.
         */

        for (int i = 0; i < conejos.length; i++) {
            conejos[i] = new Animal("Conejo", i + 1, DESCANSO_CONEJO);
            conejos[i].setPriority(Thread.NORM_PRIORITY);
            conejos[i].start();
        }

        for (int i = 0; i < guepardos.length; i++) {
            guepardos[i] = new Animal("Guepardo", i + 1, DESCANSO_GUEPARDO);
            guepardos[i].setPriority(Thread.MAX_PRIORITY);
            guepardos[i].start();
        }

        for (int i = 0; i < tortugas.length; i++) {
            tortugas[i] = new Animal("Tortuga", i + 1, DESCANSO_TORTUGA);
            tortugas[i].setPriority(Thread.MAX_PRIORITY);
            tortugas[i].start();
        }
    }
}


