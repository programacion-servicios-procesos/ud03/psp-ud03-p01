public class ThreadHorse extends Thread {

    private int contador;
    private int thread;
    private int METROS = 100;
    final int LINEA_DE_META = 5000;

    public ThreadHorse(int thread) {
        this.thread = thread;
        System.out.println("El caballo " + thread + " empieza la carrera...");
    }

    @Override
    public long getId() {
        return thread;
    }

    public void run() {
        contador = 0;

        while (contador <= LINEA_DE_META) {
            try {
                if (LINEA_DE_META - contador != 0)
                    System.out.println("Caballo " + thread + ": " + (LINEA_DE_META - contador) + "m para finalizar");

                contador += METROS;

                sleep(200);
            } catch (InterruptedException e) {
                System.err.println("El caballo " + thread + " recibe un golpe de remo y es descalificado");
                return;
            }
            if (contador == LINEA_DE_META) {
                System.out.println("Caballo " + thread + " ha finalizado la carrera");
            }
        }
    }
}
